-- hi

SELECT * FROM movies;

SELECT * FROM movies;

INSERT INTO movies (title, release_date, price)
VALUES ('batman returns', '2020-12-16 20:21:00', 45.5);

INSERT INTO movies (title, release_date, price)
VALUES ('batman returns', '2020-12-16 20:21:00', 45.5);

UPDATE movies
--SET release_date = '2020-12-16 20:21:30.5'
SET country_id=1
WHERE ID = 1;

INSERT INTO country (name) values ('ISRAEl');
INSERT INTO country (name) values ('USA');
INSERT INTO movies (title, release_date, price,  country_id)
VALUES ('batman returns', '2020-12-16 20:21:00', 45.5, 1);
INSERT INTO movies (title, release_date, price, country_id)
VALUES ('Womnder woman legend', '2020-12-20 21:20:22', 49.5, 2);

SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
    join country c on movies.country_id = c.id;

-- add table country
-- id PK AutoIncrement
-- name Unique
-- in movies table add country_id column as FK (for country table)
-- update the movie row with a country_id
-- run select + join

-- view
CREATE VIEW all_movies AS
    SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
    join country c on movies.country_id = c.id;

select * from all_movies;


-- view movies from israel --> israeli_movies
-- select ...

create function sp_get_all_movies()
    returns TABLE(id bigint, title text, release_date timestamp, price double precision, name text )
    language plpgsql AS
    $$
    BEGIN
        return query
            SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
            join country c on movies.country_id = c.id;
    END;
    $$;

select * from sp_get_all_movies();

create function sp_get_all_movies_by_country(_country_name text)
    returns TABLE(id bigint, title text, release_date timestamp, price double precision, name text )
    language plpgsql AS
    $$
    BEGIN
        return query
            SELECT movies.id, movies.title, movies.release_date, movies.price, c.name FROM movies
            join country c on movies.country_id = c.id
            where c.name = _country_name;
    END;
    $$;

select * from sp_get_all_movies_by_country('ISRAEl')

-- sp_get_all_movies between price (_min, _max)





